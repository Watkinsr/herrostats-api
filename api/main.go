package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

// Response to send back to client
type Response struct {
	Apg string `json:"apg"`
	Ppg string `json:"ppg"`
	Rpg string `json:"rpg"`
	Spg string `json:"spg"`
}

// StatsResponse NBA API Response
type StatsResponse struct {
	Internal struct {
		PubDateTime             string `json:"pubDateTime"`
		IgorPath                string `json:"igorPath"`
		Xslt                    string `json:"xslt"`
		XsltForceRecompile      string `json:"xsltForceRecompile"`
		XsltInCache             string `json:"xsltInCache"`
		XsltCompileTimeMillis   string `json:"xsltCompileTimeMillis"`
		XsltTransformTimeMillis string `json:"xsltTransformTimeMillis"`
		ConsolidatedDomKey      string `json:"consolidatedDomKey"`
		EndToEndTimeMillis      string `json:"endToEndTimeMillis"`
	} `json:"_internal"`
	League struct {
		Standard struct {
			TeamID string `json:"teamId"`
			Stats  struct {
				Latest struct {
					SeasonYear    int    `json:"seasonYear"`
					SeasonStageID int    `json:"seasonStageId"`
					Ppg           string `json:"ppg"`
					Rpg           string `json:"rpg"`
					Apg           string `json:"apg"`
					Mpg           string `json:"mpg"`
					Topg          string `json:"topg"`
					Spg           string `json:"spg"`
					Bpg           string `json:"bpg"`
					Tpp           string `json:"tpp"`
					Ftp           string `json:"ftp"`
					Fgp           string `json:"fgp"`
					Assists       string `json:"assists"`
					Blocks        string `json:"blocks"`
					Steals        string `json:"steals"`
					Turnovers     string `json:"turnovers"`
					OffReb        string `json:"offReb"`
					DefReb        string `json:"defReb"`
					TotReb        string `json:"totReb"`
					Fgm           string `json:"fgm"`
					Fga           string `json:"fga"`
					Tpm           string `json:"tpm"`
					Tpa           string `json:"tpa"`
					Ftm           string `json:"ftm"`
					Fta           string `json:"fta"`
					PFouls        string `json:"pFouls"`
					Points        string `json:"points"`
					GamesPlayed   string `json:"gamesPlayed"`
					GamesStarted  string `json:"gamesStarted"`
					PlusMinus     string `json:"plusMinus"`
					Min           string `json:"min"`
					Dd2           string `json:"dd2"`
					Td3           string `json:"td3"`
				} `json:"latest"`
				CareerSummary struct {
					Tpp          string `json:"tpp"`
					Ftp          string `json:"ftp"`
					Fgp          string `json:"fgp"`
					Ppg          string `json:"ppg"`
					Rpg          string `json:"rpg"`
					Apg          string `json:"apg"`
					Bpg          string `json:"bpg"`
					Mpg          string `json:"mpg"`
					Spg          string `json:"spg"`
					Assists      string `json:"assists"`
					Blocks       string `json:"blocks"`
					Steals       string `json:"steals"`
					Turnovers    string `json:"turnovers"`
					OffReb       string `json:"offReb"`
					DefReb       string `json:"defReb"`
					TotReb       string `json:"totReb"`
					Fgm          string `json:"fgm"`
					Fga          string `json:"fga"`
					Tpm          string `json:"tpm"`
					Tpa          string `json:"tpa"`
					Ftm          string `json:"ftm"`
					Fta          string `json:"fta"`
					PFouls       string `json:"pFouls"`
					Points       string `json:"points"`
					GamesPlayed  string `json:"gamesPlayed"`
					GamesStarted string `json:"gamesStarted"`
					PlusMinus    string `json:"plusMinus"`
					Min          string `json:"min"`
					Dd2          string `json:"dd2"`
					Td3          string `json:"td3"`
				} `json:"careerSummary"`
				RegularSeason struct {
					Season []struct {
						SeasonYear int `json:"seasonYear"`
						Teams      []struct {
							TeamID       string `json:"teamId"`
							Ppg          string `json:"ppg"`
							Rpg          string `json:"rpg"`
							Apg          string `json:"apg"`
							Mpg          string `json:"mpg"`
							Topg         string `json:"topg"`
							Spg          string `json:"spg"`
							Bpg          string `json:"bpg"`
							Tpp          string `json:"tpp"`
							Ftp          string `json:"ftp"`
							Fgp          string `json:"fgp"`
							Assists      string `json:"assists"`
							Blocks       string `json:"blocks"`
							Steals       string `json:"steals"`
							Turnovers    string `json:"turnovers"`
							OffReb       string `json:"offReb"`
							DefReb       string `json:"defReb"`
							TotReb       string `json:"totReb"`
							Fgm          string `json:"fgm"`
							Fga          string `json:"fga"`
							Tpm          string `json:"tpm"`
							Tpa          string `json:"tpa"`
							Ftm          string `json:"ftm"`
							Fta          string `json:"fta"`
							PFouls       string `json:"pFouls"`
							Points       string `json:"points"`
							GamesPlayed  string `json:"gamesPlayed"`
							GamesStarted string `json:"gamesStarted"`
							PlusMinus    string `json:"plusMinus"`
							Min          string `json:"min"`
							Dd2          string `json:"dd2"`
							Td3          string `json:"td3"`
						} `json:"teams"`
						Total struct {
							Ppg          string `json:"ppg"`
							Rpg          string `json:"rpg"`
							Apg          string `json:"apg"`
							Mpg          string `json:"mpg"`
							Topg         string `json:"topg"`
							Spg          string `json:"spg"`
							Bpg          string `json:"bpg"`
							Tpp          string `json:"tpp"`
							Ftp          string `json:"ftp"`
							Fgp          string `json:"fgp"`
							Assists      string `json:"assists"`
							Blocks       string `json:"blocks"`
							Steals       string `json:"steals"`
							Turnovers    string `json:"turnovers"`
							OffReb       string `json:"offReb"`
							DefReb       string `json:"defReb"`
							TotReb       string `json:"totReb"`
							Fgm          string `json:"fgm"`
							Fga          string `json:"fga"`
							Tpm          string `json:"tpm"`
							Tpa          string `json:"tpa"`
							Ftm          string `json:"ftm"`
							Fta          string `json:"fta"`
							PFouls       string `json:"pFouls"`
							Points       string `json:"points"`
							GamesPlayed  string `json:"gamesPlayed"`
							GamesStarted string `json:"gamesStarted"`
							PlusMinus    string `json:"plusMinus"`
							Min          string `json:"min"`
							Dd2          string `json:"dd2"`
							Td3          string `json:"td3"`
						} `json:"total"`
					} `json:"season"`
				} `json:"regularSeason"`
			} `json:"stats"`
		} `json:"standard"`
	} `json:"league"`
}

func main() {
	// any other request will be handled by this function
	http.HandleFunc("/stats", func(rw http.ResponseWriter, r *http.Request) {
		log.Println("Running Hello Handler")

		resp, err := http.Get("http://data.nba.net/data/10s/prod/v1/2019/players/1629639_profile.json")
		if err != nil {
			print(err)
		}

		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			print(err)
		}

		var statsRes StatsResponse
		json.Unmarshal([]byte(string(body)), &statsRes)

		latest := statsRes.League.Standard.Stats.Latest

		response := Response{
			Apg: latest.Apg,
			Ppg: latest.Ppg,
			Rpg: latest.Rpg,
			Spg: latest.Spg,
		}

		b, err := json.Marshal(response)
		if err != nil {
			log.Fatal("Could not marshal")
		} else {
			fmt.Fprintf(rw, "%s", string(b))
		}
	})

	// Listen for connections on all ip addresses (0.0.0.0)
	// port 9090
	log.Println("Starting Server")
	err := http.ListenAndServe(":9090", nil)
	log.Fatal(err)
}
